require('./bootstrap');
window.Vue = require('vue');
import VueRouter from 'vue-router';
import routes from './routes';
import Vuex from 'vuex';
import dataStore from './store/store';
import {initialize} from "./helpers/general";

Vue.use(VueRouter);
Vue.use(Vuex);

const router = new VueRouter({
   //mode: 'history',
   routes
});
const store = new Vuex.Store(dataStore);
import RootComponent from './components/RootComponent';

initialize(store, router);
const app = new Vue({
    el: '#app',
    components: {RootComponent},
    template: '<RootComponent />',
    router,
    store
});
