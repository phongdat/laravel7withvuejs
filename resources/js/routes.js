import LoginComponent from './components/auth/LoginComponent';
import HomeComponent from './components/HomeComponent';

const routes = [
    {path: '/', component: HomeComponent, meta: {requiresAuth: true}},
    {path: '/login', component: LoginComponent},
];
export default routes;
