import {apiMethod} from "../helpers/apiMethod";
import {apiRequest} from "../helpers/apiRequest";

export const loginUser = (formData) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/login', apiMethod.post, formData)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}

export const getLocalUser = () => {
    const userStr = localStorage.getItem("user");
    if(!userStr) return null;
    return JSON.parse(userStr);
}
