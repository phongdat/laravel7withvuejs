import {getLocalUser} from "../helpers/auth";
const user = getLocalUser();

export default {
    state: {
        welcome: 'Authentication in with Passport',
        currentUser: user,
        isLoggedIn: false,
        loading: false,
        auth_error: null,
        customers: []
    },
    getters: {
        welcome(state) {
            return state.welcome;
        },
        isLoading: state => {
            return state.loading;
        },
        currentUser: state => {
            return state.currentUser;
        },
        auth_error: state => {
            return state.auth_error;
        }
    },
    mutations: {
        login: state => {
            state.auth_error = null;
            state.loading = true;
        },
        loginSuccess: (state, payload) => {
            console.log(payload);
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.err.data.msg.message;
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        }
    },
    actions: {
        login(context) {
            context.commit('login');
        }
    }
}

