import {setAuthorization} from "./general";

export const login = (credentials) => {
    return new Promise(
        (resolve, reject) => {
            axios.post('/api/jwt/login', credentials)
                .then(response => {
                    setAuthorization(response.data.access_token);
                    resolve(response.data);
                })
                .catch((err) => {
                    reject('Wrong email or password');
                })
        }
    )
}

export const getLocalUser = () => {
    const userStr = localStorage.getItem("user");
    if(!userStr) return null;
    return JSON.parse(userStr);
}
