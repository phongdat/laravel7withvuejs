<?php
namespace App\Services\Api;
use App\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Auth;

class AuthService {
    use ApiResponseTrait;

    public function login($request) {
        $request->validate([
            'email' => 'required',
            'password'  => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if(!Auth::attempt($credentials)) return $this->returnUnAuthorizedError(['message' => 'Unauthorized']);
        $user = Auth::user();
        $data['access_token'] = $user->createToken('myApp')->accessToken;
        $data['user'] = $user;
        return $this->returnSuccess($data);
    }

    public function logout() {

    }

}
